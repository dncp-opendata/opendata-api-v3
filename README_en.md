# [Pitogüé](https://es.wikipedia.org/wiki/Pitangus_sulphuratus): DNCP Open Data Portal API version 3

![Image of pitogue](https://1.bp.blogspot.com/-jQ4wlFDa2oQ/VgqNOuDZnJI/AAAAAAAAJb4/824TAux6gNM/s1600/IMG_1335-web%2B%25281%2529.jpg)

REST service application [flask]([https://palletsprojects.com/p/flask/](https://palletsprojects.com/p/flask/))
that connects to an [ElasticSearch]([https://www.elastic.co/es/](https://www.elastic.co/es/))
to make queries to it taking into account the data sets exposed by the DNCP, including releases and records OCDS.

## Requirements

To deploy the application you need:
- `` docker `` and `` docker-compose`` or
- `` python3`` and `` virtual env``

In addition, a `` postgresql`` database with user tables is required if you want to use the application with access 
token generation

The ElasticSearch database must have at least two indexes created to work with OCDS:

- releases: in which **valid OCDS releases** must be stored
- records: in which **valid OCDS records** must be stored

## Installation

### With docker
- Modify the docker-compose variables to point to the existing elasticsearch and postgresql databases
- Execute the command ``` docker-compose up -d```
- Access the API's swagger documentation through [http://localhost:5000/data/api/v3/doc/](http://localhost:5000/data/api/v3/doc/)

### With virtual env
- Create a virtual environment by running: ```virtualenv -p python3 .ve```
- Activate the virtual environment with  ``` source .ve / bin / activate```
- Install project dependencies with ``` pip install -r requirements.txt```
- Set the environment variables DATABASE_URL and ELASTIC_URL for example:
    ```export DATABASE_URL=test``` 
    ```export ELASTIC_URL=http://172.17.0.1:9200```

- Deploy the project with ``` python3 main.py run```
- Access the API's swagger documentation through [http://localhost:5000/data/api/v3/doc/](http://localhost:5000/data/api/v3/doc/)
