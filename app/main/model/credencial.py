from .. import db


class Credencial:

    @staticmethod
    def check_credential(request_token):
        result = db.session.execute("SELECT customer_key FROM opendata.credencial WHERE regexp_replace(encode("
                                    "(customer_key || ':' || customer_secret)::bytea, 'base64'), E'[\\n\\r]+', '', 'g')"
                                    "= :val", {'val': request_token})
        result_set = result.fetchall()
        return len(result_set) > 0

