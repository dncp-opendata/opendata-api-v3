from app.main.util.elasticsearch_queries import ElasticSearchQuery


def get_modalidades(pagination):
    modalidades_query = ElasticSearchQuery(index='modalidades')
    data = modalidades_query.get_data(pagination=pagination)
    return data.data, data.status


def get_categorias(pagination):
    categorias_query = ElasticSearchQuery(index='categorias')
    data = categorias_query.get_data(pagination=pagination)
    return data.data, data.status


def get_parametros(dominio=None, pagination=None):
    query = ElasticSearchQuery(index='parametros')
    matchs = [{'field': 'dominio', 'value': dominio}] if dominio else None
    data = query.get_data(pagination=pagination, matchs=matchs)
    return data.data, data.status


def get_categoria_by_id(id=None):
    query = ElasticSearchQuery(index='categorias')
    matchs = [{'field': 'id', 'value': id}] if id else None
    return query.get_data(matchs=matchs).data['nombre']


def get_modalidad_by_id(id=None):
    query = ElasticSearchQuery(index='modalidades')
    matchs = [{'field': 'id', 'value': id}] if id else None
    return query.get_data(matchs=matchs).data['nombre']
