import math

from app.main.util.elasticsearch_queries import ElasticSearchQuery


def get_table(matchs, order, select, next, size, modulo='records', modulo_name=None):
    query = ElasticSearchQuery(index=modulo)
    data_total = query.get_data(select=select, pagination={'size': None, 'next': None})
    matchs.append({'custom_query': {
    "bool": {
        "must_not": {
            "exists": {
                "field": "compiledRelease.preQualification"
            }
        }
    }
    }})
    if modulo_name == 'convocatorias':
        matchs.append({'custom_query': {
                    "exists": {
                        "field": "compiledRelease.tender.title"
                    }
                }
            }
        )
    if next:
        next = math.ceil(next/(size if size else 10)) + 1
    data_filtered = query.get_data(pagination={'size': size if size else 10, 'next': next}, matchs=matchs,
                                   select=select, sort=order, packaged=False, date_as_range=False, exact=False)

    if data_filtered.status == 404:
        return {'data': [], 'recordsFiltered': 0, 'recordsTotal': data_total.total}
    return {'data': data_filtered.data['list'], 'recordsFiltered': data_filtered.total,
            'recordsTotal': data_total.total}
