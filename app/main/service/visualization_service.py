import datetime

from app.main.service.convocantes_service import get_convocante
from app.main.service.parametros_service import get_categoria_by_id, get_modalidad_by_id
from app.main.util.elasticsearch_queries import ElasticSearchQuery, get_or_query

release_query = ElasticSearchQuery(index='releases')
record_query = ElasticSearchQuery(index='records')


def get_minimal_info(licitacion_id):
    matchs = [{'field': 'planning.identifier', 'value': licitacion_id.strip()}]
    select = ["ocid", "tender.datePublished", "planning.estimatedDate",
              "tender.status", "tender.id", "tender.status", "tag",
              "awards.date", "contracts.dateSigned", "contracts.value", "awards.suppliers.name",
              "tender.statusDetails", "tender.title", "contracts.awardID", "contracts.id", "awards.id",
              "tender.procuringEntity.name", "contracts.status", "contracts.statusDetails", "contracts"
                                                                                            ".amendments"]
    releases = release_query.get_data(matchs=matchs,
                                      select=select, sort={'field': 'date', 'order': 'desc'},
                                      pagination={'next': None,
                                                  'size': 10})
    print(releases.data)
    if releases.status == 404:
        return releases.data, releases.status
    ultimo_release = releases.data['releases'][0]
    if 'awards' in ultimo_release and 'contracts' in ultimo_release:
        for award in ultimo_release['awards']:
            for contract in ultimo_release['contracts']:
                if contract['awardID'] == award['id']:
                    contract['suppliers'] = award['suppliers']
    releases = releases.data['releases']
    if len(releases) > 1:
        ultimo_release['previousTenders'] = []
        iter_releases = iter(releases)
        next(iter_releases)
        for release in iter_releases:
            if 'tender' in release['tag']:
                ultimo_release['previousTenders'].append({
                    'id': release['tender']['id'],
                    'statusDetails': release['tender']['statusDetails'],
                    'status': release['tender']['status'],
                    'datePublished': release['tender']['datePublished']
                })
    print(ultimo_release)
    return ultimo_release


def get_contracts_by_year_and_buyer(buyer_id, year):
    matchs = [{'field': 'compiledRelease.buyer.id', 'value': "DNCP-SICP-CODE-%d" % buyer_id},
              {'field': 'compiledRelease.contracts.dateSigned', 'value_min': '%d-01-01' % year,
               'value_max': '%d-12-31' % year}]
    records = record_query.get_data(matchs=matchs,
                                    select=["ocid", "tender.mainProcurementCategoryDetails", "tender"
                                                                                             ".title",
                                            "tender.procurementMethod",
                                            "tender.procurementMethodDetails",
                                            "tender.mainProcurementCategory", "contracts.id",
                                            "contracts.dateSigned", "contracts.value",
                                            "buyer.id", "buyer.name", "contracts.dncpContractCode",
                                            "tender.id", "awards.suppliers", "contracts.awardID",
                                            "awards.id"],
                                    sort={'field': 'compiledRelease.date', 'order': 'desc'},
                                    pagination={'next': None, 'size': 250})

    if records.status == 404:
        return records.data, records.status
    records = records.data['records']
    contracts_to_return = {'contracts': [], 'total': 0, 'year': year, 'buyer': records[0]['compiledRelease']['buyer']}
    for record in records:
        record = record['compiledRelease']
        for award in record['awards']:
            for contract in record['contracts']:
                if contract['awardID'] == award['id']:
                    contract['suppliers'] = award['suppliers']
        for contract in record['contracts']:
            contract['tender'] = record['tender']
            contracts_to_return['contracts'].append(contract)
            contracts_to_return['total'] += 1
    return contracts_to_return


def get_contracts_by_year_and_entity_level(entity_level, year):
    matchs = [{'field': 'compiledRelease.parties.details.level', 'value': entity_level},
              {'field': 'compiledRelease.contracts.dateSigned', 'value_min': '%d-01-01' % year,
               'value_max': '%d-12-31' % year}]
    records = record_query.get_data(matchs=matchs,
                                    select=["ocid", "buyer.id", "buyer.name", "parties.details.level",
                                            "contracts.dateSigned", "contracts.id"],
                                    sort={'field': 'compiledRelease.contracts.dateSigned',
                                          'order': 'desc'},
                                    pagination={'next': None, 'size': 250})

    if records.status == 404:
        return records.data, records.status
    contracts_to_return = []
    a_ret = {}
    keys = []
    for record in records.data['records']:

        record = record['compiledRelease']
        for contract in record['contracts']:
            if 'dateSigned' in contract:
                mes = datetime.datetime.strptime(contract['dateSigned'], "%Y-%m-%dT%H:%M:%S-04:00").month
                key = '{}-{}'.format(mes, record['buyer']['id'])
                if key not in a_ret:
                    a_ret[key] = {'total': 0, 'month': mes, 'buyer': record['buyer'], 'level': entity_level,
                                  'year': year}
                    keys.append(key)
                a_ret[key]['total'] = a_ret[key]['total'] + 1
    for key in keys:
        contracts_to_return.append(a_ret[key])
    return contracts_to_return


def get_name_by_code(code_list, type, matchs, field_name):
    parsed_code_list = code_list.split(',')
    codes = []
    for item in parsed_code_list:
        if type == 'CATEGORIA':
            codes.append(get_categoria_by_id(item))
        elif type == 'MODALIDAD':
            codes.append(get_modalidad_by_id(item))
        elif type == 'CONVOCANTE':
            codes.append(get_convocante(item)['name'])
    matchs.append(get_or_query(codes, field_name))


def get_base_open_tender_query(mainProcurementCategoryDetails, procurementMethodDetails, buyer, tenderPeriod_from,
                               tenderPeriod_until):
    matchs = [{'field': 'compiledRelease.tender.status', 'value': 'active'}]
    if mainProcurementCategoryDetails:
        get_name_by_code(mainProcurementCategoryDetails, 'CATEGORIA', matchs, 'compiledRelease.tender'
                                                                              '.mainProcurementCategoryDetails')
    if procurementMethodDetails:
        get_name_by_code(procurementMethodDetails, 'MODALIDAD', matchs, 'compiledRelease.tender'
                                                                        '.procurementMethodDetails')
    if buyer:
        get_name_by_code(buyer, 'CONVOCANTE', matchs, 'compiledRelease.buyer.name')
    if tenderPeriod_from or tenderPeriod_until:
        matchs.append({'field': 'compiledRelease.tender.tenderPeriod.endDate', 'value_min': tenderPeriod_from,
                       'value_max': tenderPeriod_until})
    return matchs


def get_number_of_open_tenders(mainProcurementCategoryDetails=None, procurementMethodDetails=None, buyer=None,
                               tenderPeriod_from=None, tenderPeriod_until=None):
    matchs = get_base_open_tender_query(mainProcurementCategoryDetails, procurementMethodDetails, buyer,
                                        tenderPeriod_from, tenderPeriod_until)
    data = record_query.get_data(matchs=matchs,
                                 aggregation={
                                     "open_tenders": {
                                         "date_histogram": {
                                             "field": "compiledRelease.tender.tenderPeriod.endDate",
                                             "interval": "day"
                                         }
                                     }
                                 },
                                 select=["aggregations"],
                                 sort={'field': 'compiledRelease.tender.tenderPeriod.endDate',
                                       'order': 'asc'},
                                 pagination={'next': None, 'size': 10000})
    if data.status == 404:
        return data.data, data.status
    dates = data.data['open_tenders']['buckets']
    a_ret = []
    for date in dates:
        if date['doc_count'] > 0:
            a_ret.append({'total': date['doc_count'], 'tender': {'tenderPeriod': {'endDate': date['key_as_string']}}})
    return a_ret


def get_minimal_tender(mainProcurementCategoryDetails=None, procurementMethodDetails=None, buyer=None,
                       tenderPeriod_from=None, tenderPeriod_until=None):
    matchs = get_base_open_tender_query(mainProcurementCategoryDetails, procurementMethodDetails, buyer,
                                        tenderPeriod_from, tenderPeriod_until)
    select = ['tender.id', 'tender.title', 'tender.tenderPeriod.endDate']
    data = record_query.get_data(matchs=matchs, select=select,
                                 pagination={'next': None, 'size': 10000}, packaged=False)
    if data.status == 404:
        return data.data, data.status
    return data.data, data.status
