from elasticsearch import Elasticsearch


from app.main.util.elasticsearch_queries import ElasticSearchQuery

convocantes_query = ElasticSearchQuery(index='convocantes')


def get_convocantes_list():
    return convocantes_query.get_data(pagination={'size': 1000, 'next': None}).data


def get_convocante(id):
    matchs = [{'field': 'identifier.id', 'value': id}]
    return convocantes_query.get_data(matchs=matchs).data


def buscador_convocantes(args, pagination=None):
    matchs = []
    for arg in args.keys():
        if arg in ['page', 'items_per_page']:
            continue
        matchs.append({'field': arg, 'value': args.get(arg)})

    data = convocantes_query.get_data(matchs=matchs, pagination=pagination)
    return data.data, data.status
