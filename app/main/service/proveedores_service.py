from app.main.util.elasticsearch_queries import ElasticSearchQuery, get_or_query

proveedores_query = ElasticSearchQuery(index='proveedores')
records_query = ElasticSearchQuery(index='records')


def get_proveedor(id):
    matchs = [{'field': 'identifier.id', 'value': id}]
    return proveedores_query.get_data(matchs=matchs, exact=True).data


def buscador_proveedores(args, pagination=None):
    proveedores_matchs = []
    multiple = ['details.categories.id', 'details.sanciones.type']
    for arg in args.keys():
        if arg in ['page', 'items_per_page']:
            continue
        elif arg in multiple:
            proveedores_matchs.append(get_or_query(args.get(arg).split(','), arg))
        else:
            proveedores_matchs.append({'field': arg, 'value': args.get(arg)})

    proveedores = proveedores_query.get_data(matchs=proveedores_matchs, pagination=pagination)
    if proveedores.status != 200:
        return proveedores.data, proveedores.status
    for proveedor in proveedores.data['list']:
        adjudicaciones = records_query.get_data(matchs=[
            {'field': 'compiledRelease.awards.suppliers.id', 'value': proveedor['id']}
        ])
        proveedor['cantidad_adjudicaciones'] = adjudicaciones.total
    return proveedores.data, proveedores.status
