from app.main.util.elasticsearch_queries import ElasticSearchQuery
from elasticsearch import Elasticsearch


def get_catalogo_list(pagination, tipo):
    query = ElasticSearchQuery(index=tipo)
    data = query.get_data(pagination=pagination)
    return data.data, data.status


def get_catalogo_by_id(tipo, id):
    query = ElasticSearchQuery(index=tipo)
    matchs = [{'field': 'id', 'value': id}]
    data = query.get_data(matchs=matchs)
    return data.data, data.status


def get_nivel5_by_nivel4(nivel_4_id, pagination):
    query = ElasticSearchQuery(index='catalogo_n5')
    matchs = [{'field': 'n4_codigo', 'value': nivel_4_id}]
    data = query.get_data(matchs=matchs, pagination=pagination)
    return data.data, data.status


def get_objeto_by_nivel5(id, pagination):
    query = ElasticSearchQuery(index='catalogo_n5_objeto_gasto')
    matchs = [{'field': 'id', 'value': id}]
    data = query.get_data(matchs=matchs, pagination=pagination)
    return data.data, data.status


def buscador_catalogo_n5(args, pagination=None):
    matchs = []
    query = ElasticSearchQuery(index='catalogo_n5')
    for arg in args.keys():
        if arg in ['page', 'items_per_page']:
            continue
        matchs.append({'field': arg, 'value': args.get(arg)})

    data = query.get_data(matchs=matchs, pagination=pagination)
    return data.data, data.status
