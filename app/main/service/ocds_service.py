from app.main.util.elasticsearch_queries import ElasticSearchQuery, get_or_query

release_query = ElasticSearchQuery(index='releases')
record_query = ElasticSearchQuery(index='records')

multiple = ['tender.procurementMethodDetails', 'awards.items.classification.id']


def search_releases_by_ocid(ocid, pagination):
    return release_query.get_data(matchs=[{'field': 'ocid', 'value': ocid}], pagination=pagination,
                                  sort={'field': 'date', 'order': 'desc'})


def search_releases_by_id(id):
    return release_query.get_data(matchs=[{'field': 'id', 'value': id}], pagination={'next': None, 'size': 10},
                                  sort={'field': 'date', 'order': 'desc'})


def general_search(field_values, pagination, year):
    matchs = [{'field': 'planning.estimatedDate', 'value_min': '%s-01-01' % year,
               'value_max': '%s-12-31' % year}]
    if 'search' in field_values:
        matchs.extend(field_values['search'])
    sort = field_values['sort'] if 'sort' in field_values else {'field': 'date', 'order': 'desc'}
    return release_query.get_data(matchs=matchs, pagination=pagination,
                                  sort=sort).data


def search_records_by_ocid(ocid):
    return record_query.get_data(matchs=[{'field': 'ocid', 'value': ocid}], pagination={'next': None, 'size': 10})


def general_record_search(field_values, pagination, year):
    matchs = [{'field': 'compiledRelease.planning.estimatedDate', 'value_min': '%s-01-01' % year,
               'value_max': '%s-12-31' % year}]
    if 'search' in field_values:
        matchs.extend(field_values['search'])
    sort = field_values['sort'] if 'sort' in field_values else {'field': 'compiledRelease.date', 'order': 'desc'}
    return record_query.get_data(matchs=matchs, pagination=pagination, sort=sort).data


def get_stage_by_id(stage, id, select):
    id_name = 'id' if 'planning' not in stage else 'identifier'
    matchs = [{'field': 'compiledRelease.{}.{}'.format(stage, id_name),
               'value': id}]
    if select:
        select = select.split(',')
        select = ['{}.'.format(stage) + s.strip() for s in select]
    else:
        select = ['{}.*'.format(stage)]
    select.append('{}.{}'.format(stage, id_name))
    result = record_query.get_data(matchs=matchs, select=select)
    if result.status != 200:
        return result.data, result.status
    if stage in ['contracts', 'awards', 'complaints']:
        for item in result.data[stage]:
            if item['id'] == id:
                return {stage: [item]}
        return {
            'message': 'No encontrado',
            'status': 404
        }, 404
    else:
        return result.data


def buscador_items_adjudicados(codigo, anio, pagination):
    matchs = [{'field': 'compiledRelease.awards.items.classification.id', 'value': codigo},
              {'field': 'compiledRelease.awards.date', 'value_min': '%s-01-01' % anio,
               'value_max': '%s-12-31' % anio}
              ]
    select = ['awards.items', 'awards.date', 'awards.id', 'ocid']
    items = record_query.get_data(matchs=matchs, select=select, pagination=pagination)
    if items.status == 404:
        return items.data, 404
    aret = []
    awards_aret = {"list": [], "pagination": items.data['pagination']}
    for record in items.data['records']:
        award_aret = {"ocid": record['compiledRelease']["ocid"], 'awards': []}
        awards = record['compiledRelease']['awards']
        for award in awards:
            award_aret['awards'].append(award)
            for item in award['items']:
                if item['classification']['id'] == codigo:
                    aret.append(item)
            if aret:
                award['items'] = aret
                award_aret['awards'].append(award)
                aret = []
        awards_aret['list'].append(award_aret)
    return awards_aret


def buscador_records(args, pagination=None):
    matchs = []
    for arg in args.keys():
        if arg in ['page', 'items_per_page']:
            continue
        if arg not in ['tipo_fecha', 'fecha_desde', 'fecha_hasta']:
            key = 'compiledRelease.' + arg

            if arg in multiple:
                matchs.append(get_or_query(args.get(arg).split(','), key))
            else:
                matchs.append({'field': key, 'value': args.get(arg)})
        elif arg == 'tipo_fecha' and (args.get('fecha_desde') or args.get('fecha_hasta')):
            tipo_fecha = args.get('tipo_fecha')
            if tipo_fecha == 'entrega_ofertas':
                ocds_field = 'compiledRelease.tender.tenderPeriod.endDate'
            elif tipo_fecha == 'adjudicacion':
                ocds_field = 'compiledRelease.awards.date'
            elif tipo_fecha == 'publicacion_llamado':
                ocds_field = 'compiledRelease.tender.datePublished'
            else:
                ocds_field = 'compiledRelease.contracts.dateSigned'
            matchs.append({'field': ocds_field, 'value_min': args.get('fecha_desde'),
                          'value_max': args.get('fecha_hasta')})

    return record_query.get_data(matchs=matchs, pagination=pagination,
                                 select=["tender.id", "tender.title",
                                         "planning.identifier", "contracts.id",
                                         "awards.id", "awards.suppliers",
                                         "tender.procurementMethodDetails",
                                         "tender.statusDetails", "buyer",
                                         "tender.procuringEntity",
                                         "tender.mainProcurementCategoryDetails",
                                         "tender.tenderPeriod", "tender.coveredBy",
                                         "complaints.id",
                                         "ocid", "releases"])
