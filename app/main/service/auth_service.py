import datetime

import jwt

from app.main.config import key
from app.main.model.credencial import Credencial


class Auth:

    @staticmethod
    def login_user(data):
        try:
            request_token = data.get('request_token')
            if Credencial.check_credential(request_token):
                access_token = Auth.generate_access_token(request_token)
                response_object = {
                    'access_token': access_token.decode()
                }
                return response_object, 200
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'request_token does not match.'
                }
                return response_object, 401

        except Exception as e:
            print(e)
            response_object = {
                'status': 'fail',
                'message': 'Try again'
            }
            return response_object, 500

    @staticmethod
    def generate_access_token(request_token):
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=15),
            'iat': datetime.datetime.utcnow(),
            'sub': request_token
        }
        return jwt.encode(
            payload,
            key,
            algorithm='HS256'
        )

    @staticmethod
    def get_logged_in_user(new_request):
            auth_token = new_request.headers.get('Authorization')
            if auth_token:
                resp = Auth.decode_auth_token(auth_token)
                if not isinstance(resp, str):
                    response_object = {
                        'status': 'success',
                        'data': auth_token
                    }
                    return response_object, 200
                response_object = {
                    'status': 'fail',
                    'message': resp
                }
                return response_object, 401
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Provide a valid auth token.'
                }
                return response_object, 401

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            jwt.decode(auth_token, key)
            return 0
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'
