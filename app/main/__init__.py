import os

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_gzip import Gzip
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_sqlalchemy import SQLAlchemy

from app.main.config import config_by_name

db = SQLAlchemy()
flask_bcrypt = Bcrypt()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    flask_bcrypt.init_app(app)

    return app


app = create_app(os.getenv('APP_ENV') or 'dev')

cors = CORS(app)
gzip = Gzip(app)

limiter = Limiter(
    app,
    key_func=get_remote_address,
    headers_enabled=True
)

