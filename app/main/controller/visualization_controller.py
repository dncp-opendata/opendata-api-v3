from flask import request
from flask_restplus import Resource, Namespace

from app import limiter
from app.main.service import visualization_service

api = Namespace('visualizations', description='Servicios con información resumida para visualizaciones')


@api.route('/minimal/compiledRelease/<ocid>')
class CicloLicitacion(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'ocid': 'El id de la liciación a buscar. '
                           'Ejemplo 365292'})
    def get(self, ocid):
        """
        Busca los datos de una licitación dado su id
        """
        return visualization_service.get_minimal_info(ocid)


@api.route("/minimal/contract/buyer/<buyer_id>/year/<year>")
class ContractsList(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'buyer_id': 'Codigo SICP de la entidad', 'year': 'Año del contrato a buscar'}
             )
    def get(self, buyer_id=None, year=None):
        return visualization_service.get_contracts_by_year_and_buyer(int(buyer_id), int(year))


@api.route("/minimal/contract/buyer/level/<buyer_level>/year/<year>")
class ContractsNumberList(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'buyer_level': 'Nivel de la entidad', 'year': 'Año del contrato a buscar'}
             )
    def get(self, buyer_level=None, year=None):
        return visualization_service.get_contracts_by_year_and_entity_level(buyer_level, int(year))


@api.route("/minimal/tenders/count")
class TendersOpenCount(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'mainProcurementCategoryDetails': 'Categoria',
                     'procurementMethodDetails': 'Tipo de procedimiento',
                     'buyer': 'Institucion a buscar', 'tenderPeriod_from': 'Fecha inicial de apertura de ofertas',
                     'tenderPeriod_until': 'Fecha hasta de apertura de ofertas'}
             )
    def get(self):
        mainProcurementCategoryDetails = request.args.get('mainProcurementCategoryDetails', None)
        procurementMethodDetails = request.args.get('procurementMethodDetails', None)
        buyer = request.args.get('buyer', None)
        tenderPeriod_from = request.args.get('tenderPeriod_from', None)
        tenderPeriod_until = request.args.get('tenderPeriod_until', None)
        return visualization_service.get_number_of_open_tenders(mainProcurementCategoryDetails, procurementMethodDetails, buyer,
                                                       tenderPeriod_from, tenderPeriod_until)


@api.route("/minimal/tenders")
class TendersOpen(Resource):
    @limiter.exempt
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'mainProcurementCategoryDetails': 'Categoria',
                     'procurementMethodDetails': 'Tipo de procedimiento',
                     'buyer': 'Institucion a buscar', 'tenderPeriod_from': 'Fecha inicial de apertura de ofertas',
                     'tenderPeriod_until': 'Fecha hasta de apertura de ofertas'}
             )
    def get(self):
        mainProcurementCategoryDetails = request.args.get('mainProcurementCategoryDetails', None)
        procurementMethodDetails = request.args.get('procurementMethodDetails', None)
        buyer = request.args.get('buyer', None)
        tenderPeriod_from = request.args.get('tenderPeriod_from', None)
        tenderPeriod_until = request.args.get('tenderPeriod_until', None)
        return visualization_service.get_minimal_tender(mainProcurementCategoryDetails, procurementMethodDetails, buyer,
                                                       tenderPeriod_from, tenderPeriod_until)
