from flask import request
from flask_restplus import Namespace, Resource

from app.main.service import parametros_service

api = Namespace('parameters', description='Parámetros')


@api.route('/procurementCategories')
class CategoriasList(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina'})
    def get(self):
        """
        Lista todas las categorias
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return parametros_service.get_categorias({'size': size, 'next': page})


@api.route('/procurementMethods')
class ModalidadesList(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina'})
    def get(self):
        """
        Lista todas las modalidades
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return parametros_service.get_modalidades({'size': size, 'next': page})


@api.route('/parameters')
class ParametrosList(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'domain': 'Dominio',
                     'page': 'Pagina', 'items_per_page': 'Tamaño de pagina'})
    def get(self):
        """
        Lista todas los parametros
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        dominio = request.args.get('domain', None)
        return parametros_service.get_parametros(dominio, pagination={'size': size, 'next': page})
