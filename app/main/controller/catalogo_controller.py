from flask import request
from flask_restplus import Namespace, Resource

from app.main.service import catalogo_service

api = Namespace('itemClassification', description='Catalogo')


@api.route('/n5/<id>')
class CatalogoN5Id(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'Id del producto'})
    def get(self, id=None):
        """
        Datos de un producto de nivel 5 por Id
        """

        return catalogo_service.get_catalogo_by_id('catalogo_n5', id)


@api.route('/n4/<id>')
class CatalogoN4Id(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'Id del producto'})
    def get(self, id=None):
        """
        Datos de un producto de nivel 4 por id
        """

        return catalogo_service.get_catalogo_by_id('catalogo_n4', id)


@api.route('/n4/<id>/n5')
class CatalogoN4IdNivel5(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error',
                        404: 'Not Found'},
             params={'id': 'Id del producto de nivel 4',
                     'page': 'Pagina', 'items_per_page': 'Tamaño de pagina'})
    def get(self, id=None):
        """
        Búsqueda de productos de nivel 5 por el código de nivel 4
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return catalogo_service.get_nivel5_by_nivel4(id, pagination={'next': page, 'size': size})


@api.route('/n5/<id>/expenditure_object')
class CatalogoN5ObjetoGasto(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error',
                        404: 'Not Found'},
             params={'id': 'Id del producto de nivel 5',
                     'page': 'Pagina', 'items_per_page': 'Tamaño de pagina'})
    def get(self, id=None):
        """
        Búsqueda de objetos de gasto de productos del nivel 5 por su codigo
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return catalogo_service.get_objeto_by_nivel5(id, pagination={'next': page, 'size': size})
