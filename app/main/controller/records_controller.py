import datetime

from flask import request
from flask_restplus import Resource

from app.main.service import ocds_service
from app.main.util.dto import ReleaseSearchDto

api = ReleaseSearchDto.api
release_search = ReleaseSearchDto.release_search


@api.route('/record/<ocid>')
class RecordByOcid(Resource):
    @api.doc("Servicio de búsqueda de record por OCID",
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'ocid': 'El ocid a buscar. El ocid está compuesto por el prefijo ocds-03ad3f y el numero de '
                             'proceso Ejemplo: ocds-03ad3f-365292-1'})
    def get(self, ocid):
        """
        Busca un record dado un OCID
        """

        data = ocds_service.search_records_by_ocid(ocid)
        if 'uri' in data.data:
            data.data['uri'] = request.url
        if 'publishedDate' in data.data:
            data.data['publishedDate'] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        return data.data, data.status
