from flask_restplus import Namespace, Resource

from app.main.service import convocantes_service

api = Namespace('procuringEntities', description='Convocantes')


@api.route('/<id>')
class Convocante(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El número SICP de la convocante a buscar. Ejemplo 179'})
    def get(self, id):
        """
        Busca los datos una convocante dado su id
        """
        return convocantes_service.get_convocante(id)
