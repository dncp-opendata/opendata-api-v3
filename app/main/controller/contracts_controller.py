from flask import request
from flask_restplus import Resource, api, Namespace

from app.main.service import ocds_service

api = Namespace('contracts', description='Contratos')


@api.route('/<id>')
class Contract(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El id del contrato a buscar. '
                           'Ejemplo 365292-daniel-galeano-godoy-1',
                     'sections': 'Si solo se quiere obtener cierta informacion del contrato se puede indicar '
                                  'las partes seguidas por coma en formato OCDS'})
    def get(self, id):
        """
        Busca los datos de un contrato dado su id
        """
        select = request.args.get('sections')
        return ocds_service.get_stage_by_id('contracts', id, select)

