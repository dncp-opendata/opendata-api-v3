import datetime

from flask import request
from flask_restplus import Resource

from app.main.service import ocds_service
from app.main.util.dto import ReleaseSearchDto

api = ReleaseSearchDto.api
release_search = ReleaseSearchDto.release_search


@api.route('/releases/ocid/<ocid>')
class ReleasesByOcid(Resource):
    @api.doc("Servicio de búsqueda de releases por OCID",
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina',
                 'ocid': 'El ocid a buscar. El ocid está compuesto por el prefijo ocds-03ad3f y el numero de '
                             'llamado Ejemplo: ocds-03ad3f-365292-1'})
    def get(self, ocid):
        """
        Busca todos los releases dado un OCID
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        data = ocds_service.search_releases_by_ocid(ocid, pagination={'next': page, 'size': size})
        if 'uri' in data.data:
            data.data['uri'] = request.url
        if 'publishedDate' in data.data:
            data.data['publishedDate'] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        return data.data, data.status


@api.route('/releases/id/<id>')
class ReleasesById(Resource):
    @api.doc("Servicio de búsqueda de releases por id", responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El ocid a buscar. El id del release a buscar'})
    def get(self, id):
        """
        Busca un release dado su id
        """

        data = ocds_service.search_releases_by_id(id)
        if 'uri' in data.data:
            data.data['uri'] = request.url
        if 'publishedDate' in data.data:
            data.data['publishedDate'] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        return data.data, data.status
