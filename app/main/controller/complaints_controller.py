from flask import request
from flask_restplus import Resource, api, Namespace

from app.main.service import ocds_service

api = Namespace('complaints', description='Protestas')


@api.route('/<id>')
class Complaint(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El id de la protesta a buscar. ',
                     'sections': 'Si solo se quiere obtener cierta informacion de la protesta se puede indicar '
                                  'las partes seguidas por coma en formato OCDS.'})
    def get(self, id):
        """
        Busca los datos de una protesta dado su id
        """
        select = request.args.get('sections')
        return ocds_service.get_stage_by_id('complaints', id, select)

