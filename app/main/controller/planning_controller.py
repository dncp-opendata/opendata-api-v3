from flask import request
from flask_restplus import Resource, api, Namespace

from app.main.service import ocds_service

api = Namespace('planning', description='Planificaciones')


@api.route('/<id>')
class Planning(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El id de la planificación a buscar. Ejemplo 361490',
                     'sections': 'Si solo se quiere obtener cierta informacion de la planificación se puede indicar '
                                  'las partes seguidas por coma en formato OCDS.'})
    def get(self, id):
        """
        Busca los datos de una planificación dado su id
        """
        select = request.args.get('sections')
        return ocds_service.get_stage_by_id('planning', id, select)
