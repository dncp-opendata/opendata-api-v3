from flask import request

from app import limiter
from app.main.service.auth_service import Auth


@limiter.request_filter
def header_whitelist():
    return Auth.get_logged_in_user(request)[1] == 200
