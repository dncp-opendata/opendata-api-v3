from flask import request
from flask_restplus import Resource, api, Namespace

from app.main.service import ocds_service

api = Namespace('tender', description='Licitaciones')


@api.route('/<id>')
class Tender(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El id de la licitacion a buscar. Ejemplo 361490-adquisicion-materiales-e-insumos-limpieza-1',
                     'sections': 'Si solo se quiere obtener cierta informacion de la convocatoria se puede indicar '
                                  'las partes seguidas por coma en formato OCDS.'})
    def get(self, id):
        """
        Busca los datos de una licitación dado su id
        """
        select = request.args.get('sections')
        return ocds_service.get_stage_by_id('tender', id, select)
