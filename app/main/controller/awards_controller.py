from flask import request
from flask_restplus import Resource, api, Namespace

from app.main.service import ocds_service

api = Namespace('awards', description='Adjudicaciones')


@api.route('/<id>')
class Awards(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El id de la adjudicacion a buscar. '
                           'Ejemplo 365292-regularizacion-asfaltica-avda-santa-teresa-mcal-lopez-juan-e-o-leary-1',
                     'sections': 'Si solo se quiere obtener cierta informacion de la adjudicación se puede indicar '
                                  'las partes seguidas por coma en formato OCDS. Por ejemplo: id, suppliers, date'})
    def get(self, id):
        """
        Busca los datos de un proveedor adjudicado dado su id
        """
        select = request.args.get('sections')
        return ocds_service.get_stage_by_id('awards', id, select)
