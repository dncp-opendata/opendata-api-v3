import datetime
from copy import copy

from flask import request
from flask_restplus import Resource, Namespace

from app.main.service import ocds_service, proveedores_service, catalogo_service, convocantes_service

api = Namespace('search', description='Buscadores')

tipos_fecha = ['entrega_ofertas', 'adjudicacion', 'publicacion_llamado', 'firma_contrato']


@api.route('/awarded-items/<codigo>/amount/<anio>')
class ItemAdjudicado(Resource):
    @api.doc(
             params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina'},
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'}
             )
    def get(self, codigo, anio):
        """
        Búsqueda de items adjudicados
        Por id de nivel 4 del item y un año de ajdudicacion'
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return ocds_service.buscador_items_adjudicados(codigo, anio, pagination={'next': page, 'size': size})


@api.route('/suppliers')
class Proveedor(Resource):
    @api.doc(params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina',
                     'identifier.id': 'Ruc del proveedor',
                     'name': 'Nombre del proveedor',
                     'details.categories.id': 'Indica la/s categoría/s a las que está inscrito el proveedor '
                                              '(cero o más, entre comas)',
                     'details.size': 'Tamaño de la empresa',
                     'details.sanctions.type': 'Busca proveedores que fueron sancionados. Se debe especificar si se quieren'
                                   'buscar las sanciones del tipo AMONESTACION o INHABILITACION o ambos '
                                   'separados por coma.'
                     },
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'}
             )
    def get(self):
        """
        Búsqueda de proveedores
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return proveedores_service.buscador_proveedores(request.args, pagination={'next': page, 'size': size})


@api.route('/procuringEntities')
class ProcuringEntities(Resource):
    @api.doc(params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina',
                     'identifier.id': 'Codigo sicp de la convocante',
                     'name': 'Nombre de la convocante',
                     'details.level': 'Nivel de la convocante. Ej: Universidades Nacionales',
                     'details.entityType': 'Tipo de entidad. Ej: Entidades Descentralizadas',
                     'details.type': 'Tipo. Ej: Unidad Contratación',
                     'memberOf.id': 'Codigo sicp de la entidad padre'
                     },
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'}
             )
    def get(self):
        """
        Búsqueda de convocantes
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return convocantes_service.buscador_convocantes(request.args, pagination={'next': page, 'size': size})


@api.route('/classification')
class CatalogoN5(Resource):
    @api.doc('Búsqueda de catálogo del nivel 5',
             params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina',
                     'id': 'Código del producto',
                     'name': 'Nombre del producto',
                     'categories.id': 'Categoria a buscar'},
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'}
             )
    def get(self):
        """
        Búsqueda de productos del catálogo del nivel 5
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        return catalogo_service.buscador_catalogo_n5(request.args, pagination={'next': page, 'size': size})


@api.route('/processes')
class Procesos(Resource):
    @api.doc('Búsqueda de procesos, que incluye todas las etapas del proceso de licitación por varios filtros',
             params={'page': 'Pagina', 'items_per_page': 'Tamaño de pagina',
                     'awards.suppliers.name': 'Razon Social del Proveedor Adjudicado',
                     'awards.suppliers.id': 'Ruc del proveedor adjudicado',
                     'parties.details.categories.id': 'Categorias del proveedor',
                     'parties.details.scale': 'Categoría Mipyme del proveedor',
                     'fecha_desde': 'Indicar en tipo de fecha',
                     'fecha_hasta': 'Indicar en tipo de fecha',
                     'tipo_fecha': 'Tipo de fecha, puede ser: entrega_ofertas, adjudicacion, '
                                   'publicacion_llamado, firma_contrato ',
                     'parties.identifier.id': 'Codigo SICP de las convocantes',
                     'tender.mainProcurementCategory': 'Categorias de la licitacion',
                     'ocid': 'Numero de licitacion',
                     'tender.title': 'Nombre de la licitacion',
                     'awards.items.classification.id': 'Código de catálogo de nivel 4 de los items adjudicados al '
                                                       'proveedor '
                                               '(cero o más, entre comas)',
                     'tender.items.classification.id': 'Código de catálogo de nivel 4 de los items del llamado'
                                                       '(cero o más, entre comas)',
                     'tender.procurementMethodDetails': 'Tipo de procedimiento de la/s licitación/ones para las que fue '
                                                 'adjudicado el proveedor (cero o más, entre comas)	',
                     'contracts.id': 'Codigo de Contratación',
                     'contracts.statusDetails': 'Estado del contrato',
                     'tender.coveredBy': 'Indica características legales de la licitacion: fonacide, agricultura_familiar, '
                                   'etc',
                     'tender.procuringEntity.name': 'Nombre de la convocante',
                     'planning.budget.budgetBreakdown.classifications.fuente_financiamiento': 'El tipo de fuente de financiamiento',
                     'planning.budget.budgetBreakdown.classifications.anio': 'El año de la linea presupuestaria',
                     'tender.statusDetails': 'Etapa en la que se encuentra la licitacion',
                     'parties.details.entityType': 'Tipo de convocante',
                     'contracts.implementation.financialProgress.breakdown.anio': 'Año de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.nivel': 'Nivel de la entidad de la linea  presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.entidad': 'Entidad de la linea  presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.tipo_programa': 'El tipo de programa de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.programa': 'El programa de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.sub_programa': 'El subprograma de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.proyecto': 'El proyecto de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.objeto_gasto': 'El objeto de gasto de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.fuente_financiamiento': 'La fuente de financiamiento de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.financiador': 'El financiador de la linea presupuestaria',
                     'contracts.implementation.financialProgress.breakdown.departamento': 'El departamento de la linea presupuestaria',
                     'planning.investmentProjects.id': 'Codigo SNIP planificado',
                     'contracts.investmentProjects.id': 'Codigo SNIP de los contratos'
                     },
             responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'}
             )
    def get(self):
        """
        Búsqueda de procesos por campos del estándar OCDS
        Se pueden especificar varios filtros por varios campos. Para la búsqueda se tienen en cuenta todos los filtros
        pasados como parámetros.
        El resultado es un record package OCDS
        """
        page = request.args.get('page', type=int, default=1)
        size = request.args.get('items_per_page', type=int, default=10)
        if (request.args.get('page') and request.args.get('items_per_page') and len(request.args) == 2)\
                or ((request.args.get('page') or request.args.get('items_per_page')) and len(request.args) == 1):
            return {
                       'message': 'Al menos un filtro es requerido',
                       'status': 400
                   }, 400
        tipo_fecha = request.args.get('tipo_fecha')

        if tipo_fecha and tipo_fecha not in tipos_fecha:
            return {
                       'message': 'El tipo de fecha debe ser {}'.format(tipos_fecha),
                       'status': 400
                   }, 400

        data = ocds_service.buscador_records(request.args,
                                             pagination={'next': page, 'size': size})
        if 'uri' in data.data:
            data.data['uri'] = request.url
        if 'publishedDate' in data.data:
            data.data['publishedDate'] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        if 'extensions' in data.data:
            del data.data['extensions']
        return data.data, data.status
