import math
from flask import request
from flask_restplus import Namespace, Resource
from pandas.io.json import json_normalize

from app import limiter
from app.main.service.tables_service import get_table
from app.main.util.datatable_helper import get_datatable_search_params, flatten_nested_json, is_csv

api = Namespace('data', description='Servicios para Data Tables', doc=False)

no_ocds = ['catalogo_n5', 'proveedores']


@api.route('/<modulo>')
@api.hide
class DataTableSearcher(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'})
    @limiter.exempt
    def get(self, modulo='planning'):
        """
        Lista todas los parametros
        """
        if modulo == 'catalogo':
            modulo = 'catalogo_n5'
        pagina = 0 if is_csv(request) else request.args.get('start', type=int)
        size = 10000 if is_csv(request) else request.args.get('length', type=int)
        matchs, sort, start, length, campos = get_datatable_search_params(request,
                                                                          modulo=modulo if modulo in no_ocds
                                                                          else 'records')
        datos = get_table(select=campos, matchs=matchs, order=sort, next=pagina, size=size,
                          modulo=modulo if modulo in no_ocds else 'records', modulo_name=modulo)
        if modulo == 'adjudicaciones':
            datos = flatten_nested_json('awards', datos)
        if modulo == 'contratos':
            datos = flatten_nested_json('contracts', datos)
        if is_csv(request):
            df = json_normalize(datos["data"])
            return df.to_csv()
        return datos



