from flask_restplus import Namespace, Resource

from app.main.service import proveedores_service

api = Namespace('suppliers', description='Proveedores')


@api.route('/<id>')
class Proveedor(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'id': 'El ruc del proveedor a buscar. Ejemplo 1022971-0'})
    def get(self, id):
        """
        Busca los datos un proveedor dado su ruc
        """
        return proveedores_service.get_proveedor(id)
