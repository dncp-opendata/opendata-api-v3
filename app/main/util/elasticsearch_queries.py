import math
from elasticsearch import Elasticsearch

from app.main import config
from app.main.util import ocds_helper

es = Elasticsearch(config.elastic)


class ElasticResult:
    data = {}
    total = 0
    status = 200

    def __init__(self, data, total, status=200):
        self.data = data
        self.total = total
        self.status = status


class ElasticSearchQuery:
    def __init__(self, index):
        self.index = index

    def is_ocds(self):
        return self.index == 'records' or self.index == 'releases'

    def get_data(self, select=None, matchs=None, sort=None, pagination=None, aggregation=None, packaged=True,
                 date_as_range=True, exact=True):
        body = {'query': {'bool': {}}}
        if select:
            body['_source'] = select
        if aggregation:
            body['aggs'] = aggregation
        if matchs:
            body['query']['bool']['must'] = [{'bool': {'should': []}}]
            for match in matchs:
                # Si se recibe un query personalizado se usa este
                if 'custom_query' in match:
                    query = match['custom_query']
                else:
                    # si es una fecha como rango
                    if 'date' in match['field'].lower() and date_as_range:
                        query = {"range": {
                            match['field']: {
                                "gte": match['value_min'],
                                "lte": match['value_max']
                            }}}

                    # si no, si se recibe una cantidad, periodo se busca un valor exacto
                    elif 'amount' in match['field'].lower() or 'period' in match['field'].lower() or exact:
                        query = {'match_phrase': {match['field']: {'query': match['value']}}}
                    # si no es una busqueda exacta entonces se busca por parecido
                    else:
                        query = self.get_not_exact_query(match['field'], match['value'])
                # si es un or se agrega a la lista de OR
                if 'or' in match:
                    body['query']['bool']['must'][0]['bool']['should'].append(query)
                # si no a la lista de Y
                else:
                    body['query']['bool']['must'].append(query)
        if sort:
            body['sort'] = {
                sort['field']: {'order': sort['order']}
            }
        if self.index == 'records' and '_source' in body:
            body['_source'] = ['compiledRelease.' + s if 'aggregations' not in s and 'releases' not in s else s for s in
                               body['_source']]
            body['_source'].append('ocid')

        if pagination:
            data, total = self.base_paginated_search(body=body, next=pagination['next'], size=pagination['size'],
                                                     packaged=packaged)
        else:
            data, total = self.unique_search(body)
        if data is None or total == 0:
            return ElasticResult({
                'message': 'No encontrado',
                'status': 404
            }, 0, 404)
        else:
            return ElasticResult(data, total)

    def unique_search(self, body):
        data = es.search(index=self.index, body=body)
        if data['hits']['total'] == 0:
            return None, 0
        aret = data['hits']['hits'][0]['_source']
        if self.index == 'records':
            return aret['compiledRelease'], 1
        return aret, 1

    def base_paginated_search(self, body=None, next=1, size=10, packaged=True):
        if not next:
            next = 1
        if not size:
            size = 10
        data = es.search(index=self.index, body=body, size=size, from_=(next - 1) * size)
        total = data['hits']['total']
        res = data['hits']['hits']
        list_name = self.index
        if len(res) == 0:
            return None, 0
        if self.is_ocds() and packaged:
            package = ocds_helper.get_base_metadata()
            package[self.index] = []
        else:
            package = {'list': []}
            list_name = 'list'

        package['pagination'] = {'total_items': total, 'total_pages': math.ceil(total / size), 'current_page': next,
                                     'items_per_page': size, 'total_in_page': len(res)}

        if 'aggs' in body:
            return data['aggregations'], 1
        for item in res:
            if self.index == 'records' and packaged:
                package[list_name].append(item['_source'])
            elif 'compiledRelease' in item['_source']:
                package[list_name].append(item['_source']['compiledRelease'])
            else:
                package[list_name].append(item['_source'])
        return package, data['hits']['total']

    @staticmethod
    def is_digit(value):
        try:
            val = int(value)
            return True
        except ValueError:
            return False

    @staticmethod
    def get_not_exact_query(field, value):
        if 'date' not in field.lower():
            field = field + '.keyword'
        return {"filter": {
            "script": {
                "script": {
                    "source": "if (doc['{}'].value != null) doc['{}'].value.toString()."
                              "toUpperCase().indexOf(params.to_search.toUpperCase()) >=0".format(field, field),
                    "lang": "painless",
                    "params": {
                        "to_search": "{}".format(value)
                    }
                }
            }
        }}


def get_or_query(codes, field_name):
    query = {
        'field': field_name,
        'custom_query': {
            'bool': {
                'should': [
                ]
            }
        }
    }
    for code in codes:
        query['custom_query']['bool']['should'].append(
            {'match': {field_name: {'query': code, 'operator': 'and', 'max_expansions': 1}}}
        )
    return query