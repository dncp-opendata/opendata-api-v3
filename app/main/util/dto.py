from flask_restplus import Namespace, fields


class AuthDto:
    api = Namespace('oauth', description='Servicios de Autenticación')
    user_auth = api.model('Detalles de autenticación', {
        'request_token': fields.String(required=True, description='El access token')
    })


class ReleaseSearchDto:
    api = Namespace('ocds', description='Servicios del Open Contracting Data Standard')
    release_search_fields = api.model('Estructura para búsqueda de releases', {
        'field': fields.String(description='Nombre del campo en formato OCDS a filtrar. Para campos anidados se puede '
                                           'utilizar un . como separador. Ejemplo: awards.suppliers.name buscará en los'
                                           'nombres de todos los suppliers'),
        'value': fields.String(description='Valor del campo a buscar'),
        'value_max': fields.String(description='Valor máximo a buscar. Usado solamente en caso de que el campo sea del '
                                               'tipo fecha. El formato debe ser AAAA-mm-dd'),
        'value_min': fields.String(description='Valor mínimo a buscar. Usado solamente en caso de que el campo sea del '
                                               'tipo fecha. El formato debe ser AAAA-mm-dd'),
    })
    release_sort = api.model('Estructura para Ordenamiento de releases', {
        'field': fields.String("Campo por el cual se realizará la ordenació"),
        'order': fields.String("Orden de la ordenación. Puede ser desc o asc")
    })
    release_search = api.model('Búsqueda y Ordenamiento de releases', {
        'sort': fields.Nested(release_sort),
        'search': fields.List(fields.Nested(release_search_fields))
    })
