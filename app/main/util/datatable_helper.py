import re


def get_custom_filter_query(field, value):
    if 'date' not in field.lower():
        field = field + '.keyword'
    return {"bool": {"filter": {
        "script": {
            "script": {
                "source": "if (doc['{}'].value != null) doc['{}'].value.toString()."
                          "toUpperCase().indexOf(params.to_search.toUpperCase()) >=0".format(field, field),
                "lang": "painless",
                "params": {
                    "to_search": "{}".format(value)
                }
            }
        }
    }}}


def get_datatable_search_params(request, modulo):
    columnas = {}
    matchs = []
    search_general = request.args.get("search[value]")

    order_general = request.args.get("order[0][column]")
    order_dir = request.args.get("order[0][dir]")
    start = request.args.get("start")
    length = request.args.get("length")
    campos = []
    for key in request.args.keys():

        columna = re.search('columns\\[([0-9]*)\\]\\[data\\]', key)
        search = re.search('columns\\[([0-9]*)\\]\\[search\\]\\[value\\]', key)
        if columna:
            valor = columna.group(1)
            if valor not in columnas:
                columnas[valor] = {}
            field = request.args.get(key)
            if '0' in field:
                field = field.replace('.0', '')
            columnas[valor]['field'] = 'compiledRelease.' + field if modulo == 'records' else field
            campos.append(field)
        if search:
            search = search.group(1)
            if search not in columnas:
                columnas[search] = {}
            columnas[search]['value'] = request.args.get(key)
    sort = {'field': columnas.get(order_general)['field'] +
                     ('.keyword' if 'date' not in columnas.get(order_general)['field'].lower()
                                    and 'period' not in columnas.get(order_general)['field'] else ''),
        'order': order_dir}

    for key in columnas.keys():
        filter = columnas.get(key)
        if filter['value']:
            filter['custom_query'] = get_custom_filter_query(filter['field'], filter['value'])
            matchs.append(filter)
        if search_general:
            matchs.append({'field': filter['field'], 'value': search_general, 'or': True,
                           'custom_query': get_custom_filter_query(filter['field'], search_general)})

    return matchs, sort, start, length, campos


def flatten_nested_json(key_s, data):
    flattened = data.copy()
    flattened['data'] = []
    for item in data['data']:
        if key_s in item:
            for nested in item[key_s]:
                flatten_item = {key_s: {}}
                for key in nested.keys():
                    flatten_item[key_s][key] = nested.get(key)
                for key in item.keys():
                    if key != key_s:
                        flatten_item[key] = item.get(key)
                flattened['data'].append(flatten_item)
    return flattened


def is_csv(request):
    return request.headers.get('Accept') == 'application/csv'
