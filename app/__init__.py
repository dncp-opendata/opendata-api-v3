# app/__init__.py

from flask import Blueprint, Flask, url_for
from flask_restplus import Api, apidoc

from app.main import limiter, app
from .main.controller.auth_controller import api as auth_ns
from .main.controller.awards_controller import api as award_ns
from .main.controller.buscador_controller import api as buscador_ns
from .main.controller.catalogo_controller import api as catalogo_ns
from .main.controller.complaints_controller import api as complaint_ns
from .main.controller.contracts_controller import api as contract_ns
from .main.controller.convocantes_controller import api as convocantes_ns
from .main.controller.parametros_controller import api as parametros_ns
from .main.controller.planning_controller import api as planning_ns
from .main.controller.proveedores_controller import api as proveedores_ns
from .main.controller.records_controller import api as ocds_records_ns
from .main.controller.releases_controller import api as ocds_ns
from .main.controller.tables_controller import api as tables_ns
from .main.controller.tender_controller import api as tender_ns
from .main.controller.visualization_controller import api as visualizacion_ns


class MyCustomApi(Api):
    def _register_apidoc(self, app: Flask) -> None:
        conf = app.extensions.setdefault('restplus', {})
        custom_apidoc = apidoc.Apidoc('restplus_doc', 'flask_restplus.apidoc',
                                      template_folder='templates', static_folder='static',
                                      static_url_path='/datos/api/v3/doc')

        @custom_apidoc.add_app_template_global
        def swagger_static(filename: str) -> str:
            return url_for('restplus_doc.static', filename=filename)

        if not conf.get('apidoc_registered', False):
            app.register_blueprint(custom_apidoc)
        conf['apidoc_registered'] = True

blueprint = Blueprint('api', __name__, url_prefix='/datos/api/v3/doc')

authorizations = {
    'Bearer Auth': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    },
}

api = MyCustomApi(blueprint, security='Bearer Auth', authorizations=authorizations)


limiter.limit("4/second", error_message='Number of request exceeded or invalid token')(blueprint)
api.add_namespace(auth_ns)
api.add_namespace(buscador_ns)
api.add_namespace(ocds_ns)
api.add_namespace(planning_ns)
api.add_namespace(tender_ns)
api.add_namespace(award_ns)
api.add_namespace(contract_ns)
api.add_namespace(complaint_ns)
api.add_namespace(convocantes_ns)
api.add_namespace(parametros_ns)
api.add_namespace(proveedores_ns)
api.add_namespace(catalogo_ns)
api.add_namespace(tables_ns)
api.add_namespace(visualizacion_ns)
