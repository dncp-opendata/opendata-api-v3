FROM python:3.7
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt && python change_swagger_theme.py
CMD uwsgi --ini app.ini