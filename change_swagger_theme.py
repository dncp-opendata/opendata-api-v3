import flask_restplus
import os
from shutil import copyfile

# Replace default SWAGGER theme with feeling-blue theme
css_dir = os.path.abspath(os.path.join(flask_restplus.__file__, '../static'))
html_dir = os.path.abspath(os.path.join(flask_restplus.__file__, '../templates'))
copyfile('swagger/swagger-ui.html', html_dir + '/swagger-ui.html')
copyfile('swagger/swagger-ui.css', css_dir + '/swagger-ui.css')
copyfile('swagger/logo_small.png', css_dir + '/logo_small.png')
