[English](https://gitlab.com/dncp-opendata/opendata-api-v3/blob/master/README_en.md)

# [Pitogüé](https://es.wikipedia.org/wiki/Pitangus_sulphuratus): API del Portal de Datos Abiertos  de la DNCP versión 3

![Image of pitogue](https://1.bp.blogspot.com/-jQ4wlFDa2oQ/VgqNOuDZnJI/AAAAAAAAJb4/824TAux6gNM/s1600/IMG_1335-web%2B%25281%2529.jpg)

Aplicación de servicios REST [flask]([https://palletsprojects.com/p/flask/](https://palletsprojects.com/p/flask/))  que se conecta a una base de datos [ElasticSearch]([https://www.elastic.co/es/](https://www.elastic.co/es/)) para realizar consultas a la misma teniendo en cuenta los conjuntos de datos expuestos por la DNCP.
## Requerimientos

Para levantar la aplicación se necesita:
- ```docker``` y ```docker-compose``` o
- ```python3``` y ```virtual env```
Además se necesita una base de datos ```postgresql``` con las tablas de usuarios si se desea utilizar la aplicación con generación de tokens de acceso

La base de datos ElasticSearch debe tener, para funcionar con OCDS, al menos dos índices creados:

- releases: en la cual se deben guardar **releases OCDS** válidos
- records: en la cual se deben guardar **records OCDS** válidos

## Instalación

### Con docker
- Modificar las variables del docker-compose para apuntar al elasticsearch y el postgresql existente
- Ejecutar el comando ```docker-compose up -d```
- Acceder al swagger de la API a través de [http://localhost:5000/datos/api/v3/doc/](http://localhost:5000/datos/api/v3/doc/)

### Con virtual env
- Crear un entorno virtual ejecutando: ```virtualenv -p python3 .ve```
- Activar el entorno virtual con ```source .ve/bin/activate```
- Instalar las dependencias del proyecto con ```pip install -r requirements.txt```
- Setear las variables de entorno DATABASE_URL y ELASTIC_URL por ejemplo:
    ```export DATABASE_URL=test``` 
    ```export ELASTIC_URL=http://172.17.0.1:9200```

- Levantar el proyecto con ```python3 main.py run```
- Acceder al swagger de la API a través de [http://localhost:5000/datos/api/v3/doc/](http://localhost:5000/datos/api/v3/doc/)
